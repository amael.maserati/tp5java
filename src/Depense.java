public class Depense {
    /** Cet attribut modélise le montant d'une dépense */
    private double montant;
    /** Cet attribut modélise le produit d'une dépense */
    private String produit;
    /** Cet attribut modélise la personne ayant fait une dépense pour le week-end */
    private Personne payeur;
    /** permet de créer une dépense
     * @param personne la personne ayant fait un achat
     * @param montant le montant de la dépense
     * @param produit le produit acheté
     */
    public Depense(Personne personne, double montant, String produit) {
        this.montant = montant;
        this.produit = produit;
        this.payeur = personne;
    }
    /** permet d'obtenir le produit de la dépense
     * @return le produit acheté
     */
    public String getProduit() {
        return this.produit;
    }
    /** peremt d'obtenir le payeur
     * @return le payeur
     */
    public Personne getPayeur() {
        return this.payeur;
    }
    /** permet d'obtenir le montant de la dépense
     * @return le montant de la dépense
     */
    public double getMontant() {
        return this.montant;
    }
}
