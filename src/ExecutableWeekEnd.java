public class ExecutableWeekEnd {
    public static void main(String [] args) {
        WeekEnd mai = new WeekEnd("Mai");
        Personne noa = new Personne("Noa", 18);
        Personne sebastien = new Personne("Sebastien", 18);
        Depense depenseNoa = new Depense(noa, 150., "nourriture");
        Depense depenseSebastien = new Depense(sebastien, 200., "tentes");
        mai.ajouteParticipant("Noa", 18);
        mai.ajouteParticipant("Sebastien", 18);
        mai.ajouteDepense(150, "nourriture", "Noa");
        mai.ajouteDepense(200, "tentes", "Sebastien");
        // tests pour les méthodes de la classe Personne
        assert noa.getAge() == 18;
        assert noa.getNom().equals("Noa");
        // tests pour les méthodes de la classe Depense
        assert depenseNoa.getPayeur().equals(noa);
        assert depenseSebastien.getMontant() == 200;
        assert depenseSebastien.getProduit().equals("tentes");
        // tests pour les méthodes de la classe WeekEnd
        assert mai.totalDepense(noa) == 150.;
        assert mai.totalDepense(sebastien) == 200;
        assert mai.totalDepense() == 350.;
        assert mai.totalDepense("nourriture") == 150.;
    }
}