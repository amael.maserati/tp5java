public class Personne {
    /** Cet attribut modélise le prénom d'une personne */
    private String prenom;
    /** Cet attrigut modélise l'âge d'une personne */
    private int age;
    /** permet de créer une personne */
    public Personne(String prenom, int age) {
        this.prenom = prenom;
        this.age = age;
    }
    /** permet d'obtenir le prénom d'une personne
     * @return le prénom de la personne
     */
    public String getNom() {
        return this.prenom;
    }
    /** permet d'obtenir l'âge d'une personne
     * @return l'âge de la personne
     */
    public int getAge() {
        return this.age;
    }
    /** permet de comparer deux personnes */
    @Override
    public boolean equals(Object o) {
        if (o == this) { return true; }
        if (o == null) { return false; }
        if (o instanceof Personne) {
            Personne p = (Personne) o;
            return p.prenom.equals(this.prenom) && p.age == this.age;
        }
        return false;
    }
    /** permet d'écrire les informations d'une personne */
    @Override
    public String toString() {
        return "(" + this.prenom + "," + this.age + ")";
    }
}
