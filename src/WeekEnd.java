import java.util.*;

public class WeekEnd {
    /** Cet attribut modélise la date du week-end */
    private String dateDuWeekEnd;
    /** Cet attribut modélise une liste des participants du week-end */
    private List<Personne> participants;
    /** Cet attribut modélise une liste des dépenses du week-end */
    private List<Depense> lesDepenses;
    /** permet de créer un week-end
     * @param dateDuWeekEnd la date du week-end
     */
    public WeekEnd(String dateDuWeekEnd) {
        this.dateDuWeekEnd = dateDuWeekEnd;
        this.participants = new ArrayList<>();
        this.lesDepenses = new ArrayList<>();
    }
    /** permet d'ajouter un participant à la liste de participants
     * @param prenom le prénom du participant
     * @param age l'âge du participant
     */
    public void ajouteParticipant(String prenom, int age) {
        this.participants.add(new Personne(prenom, age));
    }
    /** permet d'ajouter une dépense à la liste de dépenses
     * @param montant le montant de l'achat
     * @param produit le produit acheté
     *@param prenom le prenom de la personne qui a commis l'achat
     */
    public void ajouteDepense(double montant, String produit, String prenom) {
        for (Personne p : this.participants) {
            if (p.getNom().equals(prenom)) {
                this.lesDepenses.add(new Depense(p, montant, produit));
            }
        }
    }
    /** permet de calculer les dépenses totales d'une personne lors du week-end
     * @param personne la personne en question
     * @return l'argent total dépensé par la personne
     */
    public double totalDepense(Personne personne) {
        double totalDepense = 0;
        for (Depense d : this.lesDepenses) {
            if (d.getPayeur().equals(personne)) {
                totalDepense += d.getMontant();
            }
        }
        return totalDepense;
    }
    /** permet de calculer l'argent qui a été dépensé par tous les participants du week-end
     * @return les dépenses totales
     */
    public double totalDepense() {
        double totalDepense = 0;
        for (Depense d : this.lesDepenses) {
            totalDepense += d.getMontant();
        }
        return totalDepense;
    }
    /** permet de calculer l'argent total dépensé pour l'achat d'un produit en particulier
     * @produit le produit en question
     * @return les dépenses totales concernant le produit 
     */
    public double totalDepense(String produit) {
        double totalDepense = 0;
        for (Depense d : this.lesDepenses) {
            if (d.getProduit().equals(produit)) {
                totalDepense += d.getMontant();
            }
        }
        return totalDepense;
    }
    /** permet de calculer l'avoir d'une personne pour savoir si une personne doit être remboursée ou si elle doit rembourser les autres
     * @param personne une personne
     * @return l'avoir de la personne
     */
    public double avoirPersonne(Personne personne) {
        double avoir = 0;
        for (Depense d : this.lesDepenses) {
            if (d.getPayeur().equals(personne)) {
                avoir = d.getMontant() - this.totalDepense();
            }
        }
        return avoir;
    }
}
